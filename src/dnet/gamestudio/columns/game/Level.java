package dnet.gamestudio.columns.game;

import static org.lwjgl.opengl.GL11.*;

import dnet.gamestudio.columns.Columns;
import dnet.gamestudio.columns.gui.Gui;
import dnet.gamestudio.columns.res.TextureLoader;
import org.lwjgl.input.Keyboard;

public abstract class Level {
	
	private final String backgroundTexture;
	private final int playAreaWidth;
	private final int playAreaHeight;
	/** Player 1's game area. */
	private final Gem[][] player1Tiles;
	/** Player 2's game area. */
	private final Gem[][] player2Tiles;
	private final Gem dummyGem = new Gem(6);
	
	/** Player 1's currently falling column. */
	private Column currentColumnP1;
	/** Player 1's next column. */
	private Column nextColumnP1;
	/** Player 2's currently falling column. */
	private Column currentColumnP2;
	private boolean needRemove = false;
	private boolean gameOver = false;

	private boolean wait = false;
	private long waitStartTime = 0;

	public long lastAutoFall = 0;
	private long autoFallDelay = 1000;
	private int score = 0;
	
	/** If this is a multiplayer game. */
	private boolean multiplayer = false;
	
	public Level(String backgroundTexture, int autoFallDelay, int playAreaWidth, int playAreaHeight) {
		this.backgroundTexture = backgroundTexture;
		this.autoFallDelay = autoFallDelay;
		this.playAreaWidth = playAreaWidth;
		this.playAreaHeight = playAreaHeight;
		player1Tiles = new Gem[playAreaWidth][playAreaHeight];
		player2Tiles = new Gem[playAreaWidth][playAreaHeight];
		for (int x = 0; x < playAreaWidth; x ++) {
			for (int y = 0; y < playAreaHeight; y ++) {
				player1Tiles[x][y] = new Gem(-1);
			}
		}
		TextureLoader.loadTexture("gems.png");
		TextureLoader.loadTexture(backgroundTexture);
		currentColumnP1 = genColumn();
		currentColumnP1.setX(4);
		currentColumnP1.setY(0);
		nextColumnP1 = genColumn();
		nextColumnP1.setX(0);
		nextColumnP1.setY(2);
		lastAutoFall = System.currentTimeMillis();
	}
	
	public int getPlayAreaWidth() {
		return playAreaWidth;
	}

	public int getPlayAreaHeight() {
		return playAreaHeight;
	}

	public void update() {
		if (gameOver) {
		    if (Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
                Columns.instance.startGame();
            }
		    return;
        }
		if (needRemove) {
			if (wait) {
				if (System.currentTimeMillis() - waitStartTime >= 550) {
					wait = false;
				}
			} else {
				removeGaps();
				needRemove = false;
				checkForLines();
			}
		} else {
			if (currentColumnP1 != null) {
				long time = System.currentTimeMillis();
				if (time - lastAutoFall > autoFallDelay) {
					currentColumnP1.drop();
					lastAutoFall = System.currentTimeMillis();
				}
				currentColumnP1.update(this);
			} else {
				currentColumnP1 = nextColumnP1;
				nextColumnP1 = genColumn();
				nextColumnP1.setX(0);
				nextColumnP1.setY(2);
				currentColumnP1.setX(4);
				currentColumnP1.setY(0);
			}		
		}
	}
	
	public void render() {
		renderBackground();
		renderPlayer1();
		renderPlayer2();
		if (gameOver) {
		    glColor3f(1.0f, 0.0f, 0.0f);
            Gui.fontRendererLarge.drawString("Game Over!", 128, 297);
            Gui.fontRendererLarge.drawString("Press Enter", 119, 297 + 19);
            glColor3f(1.0f, 1.0f, 1.0f);
		} else {
			Gui.fontRendererSmall.drawString("Score: " + score, 15, 15);
		}
	}
	
	private void renderBackground() {
		glBindTexture(GL_TEXTURE_2D, TextureLoader.getTexture(backgroundTexture));
		glBegin(GL_QUADS);
			glTexCoord2d(0, 0);
			glVertex2i(0, 0);
			
			glTexCoord2d(0.844d, 0);
			glVertex2i(865, 0);
			
			glTexCoord2d(0.844d, 0.586d);
			glVertex2i(865, 600);
			
			glTexCoord2d(0, 0.586d);
			glVertex2i(0, 600);
		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	private void renderPlayer1() {
		// Render the gems in the play area
		glBindTexture(GL_TEXTURE_2D, TextureLoader.getTexture("gems.png"));
		glPushMatrix();
		glTranslatef(64, 64, 0);

		glBegin(GL_QUADS);
		for (int x = 0; x < player1Tiles.length; x++) {
			for (int y = 0; y < player1Tiles[x].length; y++) {
				if (player1Tiles[x][y].getColor() != -1) {
					player1Tiles[x][y].render(x, y);
				}
                if (player1Tiles[x][y].remove) {
                    dummyGem.render(x, y);
                }
			}
		}
		glEnd();
		
		// Render the currently falling column.
		if (currentColumnP1 != null && !wait) {
			currentColumnP1.render();
		}
		
		glTranslatef(0, 0, 0);
		glPopMatrix();
		
		// Render the next coming column.
		glPushMatrix();
		glTranslatef(416, 32, 0);
		
		if (nextColumnP1 != null) {
			nextColumnP1.render();
		}
		
		glTranslatef(0, 0, 0);
		glPopMatrix();
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	private void renderPlayer2() {
		if (!multiplayer) {
			Gui.fontRendererLarge.drawString("Press F11", 590, 200);
			Gui.fontRendererLarge.drawString("To start", 598, 219);
			Gui.fontRendererLarge.drawString("Multiplayer", 566, 238);
			return;
		}
		// Render the gems in the play area
		glBindTexture(GL_TEXTURE_2D, TextureLoader.getTexture("gems.png"));
		glPushMatrix();
		glTranslatef(512, 64, 0);

		glBegin(GL_QUADS);
		for (int x = 0; x < player2Tiles.length; x++) {
			for (int y = 0; y < player2Tiles[x].length; y++) {
				if (player2Tiles[x][y].getColor() != -1) {
					player2Tiles[x][y].render(x, y);
				}
			}
		}
		glEnd();
		
		// Render the currently falling column.
		if (currentColumnP2 != null) {
			currentColumnP2.render();
		}
		
		glTranslatef(0, 0, 0);
		glPopMatrix();
	}
	
	public Gem getGem(int x, int y) {
		return player1Tiles[x][y];
	}
	
	private Column genColumn() {
		return new Column();
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
	    this.score = score;
    }
	
	public Gem[][] getPlayArea() {
		return player1Tiles;
	}
	
	public boolean canFit(Column column) {
		if (column.getY() < 2) return false;
		if (player1Tiles[column.getX()][column.getY()].getColor() == -1) return true;
		return false;
	}
	
	public void insertColumn(Column column) {
		for (int i = 0; i < 3; i ++) {
			player1Tiles[column.getX()][column.getY() - i] = column.getGem(i);
		}
		checkForLines();
	}
	
	private void removeGaps() {
		for (int x = 0; x < playAreaWidth; x ++) {
			for (int y = 0; y < playAreaHeight; y ++) {
				if (player1Tiles[x][y].getColor() == -1 || player1Tiles[x][y].remove) {
					for (int yM = y; yM > 0; yM --) {
						player1Tiles[x][yM] = player1Tiles[x][yM - 1];
					}
					player1Tiles[x][0] = new Gem(-1);
				}
			}
		}
	}
	
	private int checkForLines() {
		int linesFound = 0;
		
		// CHECK VERTICAL
		for (int x = 0; x < playAreaWidth; x++) {
			for (int y = 1; y < playAreaHeight - 1; y++) {
				if ((player1Tiles[x][y].getColor() != -1) && ((player1Tiles[x][y].getColor()) == (player1Tiles[x][y - 1].getColor())) && ((player1Tiles[x][y].getColor()) == (player1Tiles[x][y + 1].getColor()))) {
					player1Tiles[x][y].remove = true;
					player1Tiles[x][y + 1].remove = true;
					player1Tiles[x][y - 1].remove = true;
					linesFound++;
				}
			}
		}
		
		// CHECK HORIZONTAL
		for (int x = 1; x < playAreaWidth - 1; x++) {
			for (int y = 0; y < playAreaHeight; y++) {
				if ((player1Tiles[x][y].getColor() != -1) && ((player1Tiles[x][y].getColor()) == (player1Tiles[x + 1][y].getColor())) && ((player1Tiles[x][y].getColor()) == (player1Tiles[x - 1][y].getColor()))) {
					player1Tiles[x][y].remove = true;
					player1Tiles[x + 1][y].remove = true;
					player1Tiles[x - 1][y].remove = true;
					linesFound++;
				}
			}
		}
		
		// CHECK DIAGONALS
		for (int x = 1; x < playAreaWidth - 1; x++) {
			for (int y = 1; y < playAreaHeight - 1; y++) {
				if ((player1Tiles[x][y].getColor() != -1) && ((player1Tiles[x][y].getColor()) == (player1Tiles[x - 1][y + 1].getColor())) && ((player1Tiles[x][y].getColor()) == (player1Tiles[x + 1][y - 1].getColor()))) {
					player1Tiles[x][y].remove = true;
					player1Tiles[x - 1][y + 1].remove = true;
					player1Tiles[x + 1][y - 1].remove = true;
					linesFound++;
				}

				if ((player1Tiles[x][y].getColor() != -1) && ((player1Tiles[x][y].getColor()) == (player1Tiles[x - 1][y - 1].getColor())) && ((player1Tiles[x][y].getColor()) == (player1Tiles[x + 1][y + 1].getColor()))) {
					player1Tiles[x][y].remove = true;
					player1Tiles[x - 1][y - 1].remove = true;
					player1Tiles[x + 1][y + 1].remove = true;
					linesFound++;
				}

			}
		}
		
		if (linesFound > 0) {
		    needRemove = true;
		    wait = true;
		    waitStartTime = System.currentTimeMillis();
		} else {
			currentColumnP1 = null;
		}
		
		score += linesFound;

		if (score >= 10 && score <= 15) {
			this.autoFallDelay = 750;
		} else if (score >= 16 && score <= 20) {
			this.autoFallDelay = 500;
		} else if (score >= 21 && score <= 25) {
			this.autoFallDelay = 250;
		}

		return linesFound;
	}
	
	public void displose() {
		TextureLoader.unload(backgroundTexture);
	}

	public void gameOver() {
		currentColumnP1 = null;
		nextColumnP1 = null;
		gameOver = true;
        for (int x = 0; x < playAreaWidth; x ++) {
            for (int y = 0; y < playAreaHeight; y ++) {
                player1Tiles[x][y] = new Gem(7);
            }
        }
		System.out.print("GameOver");
	}

    public void setTileData(Gem[][] tileData) {
        for (int x = 0; x < playAreaWidth; x ++) {
            for (int y = 0; y < playAreaHeight; y ++) {
                player1Tiles[x][y] = tileData[x][y];
            }
        }
    }
}
