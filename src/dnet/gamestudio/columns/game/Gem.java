package dnet.gamestudio.columns.game;

import static org.lwjgl.opengl.GL11.*;

public class Gem {
	
	private int colorIndex = 0;
	
	public boolean remove = false;
	
	public Gem(int color) {
		this.colorIndex = color;
	}
	
	public void render(int xPos, int yPos) {
		if (colorIndex == -1) return;
		glTexCoord2d(0.125d * colorIndex, 0);
		glVertex2i((xPos * 32), (yPos * 32));
			
		glTexCoord2d((0.125d * colorIndex) + 0.125d, 0);
		glVertex2i((xPos * 32) + 32, (yPos * 32));
			
		glTexCoord2d((0.125d * colorIndex) + 0.125d, 1);
		glVertex2i((xPos * 32) + 32, (yPos * 32) + 32);
			
		glTexCoord2d(0.125d * colorIndex, 1);
		glVertex2i((xPos * 32), (yPos * 32) + 32);
	}
	
	public int getColor() {
		return colorIndex;
	}
	
}
