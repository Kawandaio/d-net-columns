package dnet.gamestudio.columns.game.powerup;

import dnet.gamestudio.columns.game.Gem;
import dnet.gamestudio.columns.game.Level;

/**
 * Created by Kawan on 02/03/2017.
 * Destroys an entire column.
 */
public class PowerupColumnBuster extends Powerup {

    public PowerupColumnBuster() {
        super(new Gem[] {new Gem(9), new Gem(9), new Gem(9)});
    }

    @Override
    public void onOnPlaced(Level level) {
        for (int y = 0; y < level.getPlayAreaHeight(); y ++) {
            level.getGem(getX(), y).remove = true;
        }
    }
}
