package dnet.gamestudio.columns.game.powerup;

import dnet.gamestudio.columns.game.Gem;
import dnet.gamestudio.columns.game.Level;

/**
 * Created by Kawandaio on 02/03/2017.
 * Destroys every gem of the same color as the one this powerup lands on.
 */
public class PowerupColorBuster extends Powerup {

    public PowerupColorBuster() {
        super(new Gem[] {new Gem(8), new Gem(8), new Gem(8)});
    }

    @Override
    public void onOnPlaced(Level level) {
        if (getY() == 0) return;
        Gem gem = level.getGem(getX(), getY() - 1);
        for (int x = 0; x < level.getPlayAreaWidth(); x ++) {
            for (int y = 0; y < level.getPlayAreaHeight(); y ++) {
                if (level.getGem(x, y).getColor() == gem.getColor()) {
                    level.getGem(x, y).remove = true;
                }
             }
        }
    }
}
