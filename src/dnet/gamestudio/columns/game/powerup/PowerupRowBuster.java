package dnet.gamestudio.columns.game.powerup;

import dnet.gamestudio.columns.game.Column;
import dnet.gamestudio.columns.game.Gem;
import dnet.gamestudio.columns.game.Level;

/**
 * Created by Kawan on 03/03/2017.
 */
public class PowerupRowBuster extends Powerup {

    public PowerupRowBuster(Gem[] gems) {
        super(new Gem[] {new Gem(10), new Gem(10), new Gem(10)});
    }

    @Override
    public void onOnPlaced(Level level) {
        for (int x = 0; x < level.getPlayAreaWidth(); x ++) {
            level.getGem(x, getY()).remove = true;
        }
    }
}
