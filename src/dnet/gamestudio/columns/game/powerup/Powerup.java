package dnet.gamestudio.columns.game.powerup;

import dnet.gamestudio.columns.game.Column;
import dnet.gamestudio.columns.game.Gem;
import dnet.gamestudio.columns.game.Level;

/**
 * Created by Kawandaio (Guy Delph) on 02/03/2017.
 */
public abstract class Powerup extends Column {

    public Powerup(Gem[] gems) {
        this.gems = gems;
    }

    public abstract void onOnPlaced(Level level);

}
