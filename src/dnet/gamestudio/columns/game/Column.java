package dnet.gamestudio.columns.game;

import java.util.Random;

import dnet.gamestudio.columns.Columns;
import dnet.gamestudio.columns.sound.SoundEngine;
import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

/**
 * <p>Represents a falling column.</p>
 * @author Kawandaio (Guy Delph)
 *
 */
public class Column {
	
	/** Random number generator. */
	private static final Random random = new Random();
	
	/** The x and y position of this column. */
	private int x, y;
	
	/** The gems within this column. */
	protected Gem[] gems = new Gem[3];
	
	/** Last key-press times, used to slow key repeat times. */
	private long[] lastKeys = new long[] {0l, 0l, 0l, 0l};
	
	public Column() {
		gems[0] = new Gem(random.nextInt(5));
		gems[1] = new Gem(random.nextInt(5));
		gems[2] = new Gem(random.nextInt(5));
		x = 0;
		y = 0;
	}
	
	public void update(Level level) {
		long time = System.currentTimeMillis();
		
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT) && x != 0) {
			if (time - lastKeys[0] > 500) {
				if (level.getGem(x - 1, y).getColor() == -1) {
					x --;
					Columns.instance.getSoundEngine().play(SoundEngine.MOVE);
				}
				lastKeys[0] = System.currentTimeMillis();
			}
		} else {
			lastKeys[0] = 0;
		}
		
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT) && x != level.getPlayAreaWidth() - 1) {
			if (time - lastKeys[1] > 500) {
				if (level.getGem(x + 1, y).getColor() == -1) {
					x ++;
                    Columns.instance.getSoundEngine().play(SoundEngine.MOVE);
				}
				lastKeys[1] = System.currentTimeMillis();
			}
		} else {
			lastKeys[1] = 0;
		}
		
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			if (time - lastKeys[2] > 500) {
				shuffle();
				lastKeys[2] = System.currentTimeMillis();
			}
		} else {
			lastKeys[2] = 0;
		}
		
		if ((Keyboard.isKeyDown(Keyboard.KEY_DOWN)) && y != level.getPlayAreaHeight() - 1) {
			if (time - lastKeys[3] > 150) {
				if (level.getGem(x, y + 1).getColor() == -1) {
					y ++;
                    Columns.instance.getSoundEngine().play(SoundEngine.MOVE);
				}
				lastKeys[3] = System.currentTimeMillis();
				level.lastAutoFall = lastKeys[3];
			}
		} else {
			lastKeys[3] = 0;
		}
		
		if (y != (level.getPlayAreaHeight() - 1)) {
			if (level.getGem(x, y + 1).getColor() != -1) {
				if (level.canFit(this)) {
					level.insertColumn(this);
				} else {
					level.gameOver();
				}
			}
		} else {
			if (level.canFit(this)) {
				level.insertColumn(this);
			} else {
				level.gameOver();
			}
		}
	}
	
	private void shuffle() {
		Columns.instance.getSoundEngine().play(SoundEngine.SHUFFLE);
		final Gem[] tmp = new Gem[3];
		tmp[0] = gems[2];
		tmp[1] = gems[0];
		tmp[2] = gems[1];
		gems = tmp;
	}
	
	public void render() {
		glBegin(GL_QUADS);
		if (y == 0) {
			gems[0].render(x, y);
		} else if (y == 1) {
			gems[1].render(x, y - 1);
			gems[0].render(x, y);
		} else {
			gems[2].render(x, y - 2);
			gems[1].render(x, y - 1);
			gems[0].render(x, y);
		}
		glEnd();
	}
	
	public Gem getGem(int index) {
		return gems[index];
	}

	public void setX(int x) {
		this.x = x;
	}
	
	public int getX() {
		return x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getY() {
		return y;
	}
	
	public void drop() {
		y ++;
	}
	
}
