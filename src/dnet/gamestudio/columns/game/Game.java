package dnet.gamestudio.columns.game;

import java.io.*;

import dnet.gamestudio.columns.gui.GuiHost;
import dnet.gamestudio.columns.log.Log;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import dnet.gamestudio.columns.Columns;
import dnet.gamestudio.columns.multiplayer.MultiplayerHost;
import dnet.gamestudio.columns.multiplayer.MultiplayerJoin;

public class Game {

	private int currentLevelNumber = 0;
	private Level currentLevel;
	private boolean ready = false;
	private MultiplayerHost multiplayerHost;
	private MultiplayerJoin multiplayerJoin;
	private boolean multiplayer = false;

	private GuiHost hostGui;
	
	public Game() {}

	public Game(File saveFile) {
	    loadGame(saveFile);
    }
	
	public void startNewGame() {
		currentLevel = new Level1();
		ready = true;
	}
	
	public void hostGame() {
        Log.i("Starting multiplayer game...");
        Log.i("Mode: Host");
        multiplayer = true;
        hostGui = new GuiHost();
		try {
			multiplayerHost = new MultiplayerHost(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		multiplayerHost.start();
	}

	public void joinGame(String host, int port) {
	    Log.i("Starting multiplayer game...");
	    Log.i("Mode: Join");
	    multiplayer = true;
        try {
            multiplayerJoin = new MultiplayerJoin(host, port);
        } catch (Exception e) {
            e.printStackTrace();
        }
        multiplayerJoin.start();
    }

	public void saveGame() {
		if (multiplayer) return;
		File saveFile = new File("columns.sav");
		Document doc = new Document();
		Element root = new Element("ColumnsSave");
		doc.setRootElement(root);

		Element head = new Element("Head");
		head.setAttribute("level", "" + currentLevelNumber);
		head.setAttribute("score", "" + currentLevel.getScore());
		root.addContent(head);
		
		Element levelData = new Element("LevelData");
		Gem[][] tiles = currentLevel.getPlayArea();
		Gem gem;
		Element gemElement;
		for (int x = 0; x < currentLevel.getPlayAreaWidth(); x ++) {		
			for (int y = 0; y < currentLevel.getPlayAreaHeight(); y ++) {
				gem = tiles[x][y];
				gemElement = new Element("Gem");
				gemElement.setAttribute("x", "" + x);
				gemElement.setAttribute("y", "" + y);
				gemElement.setAttribute("color", "" + gem.getColor());
				gemElement.setAttribute("remove", "" + gem.remove);
				levelData.addContent(gemElement);
			}
		}
		root.addContent(levelData);
		
		XMLOutputter xmlOut = new XMLOutputter(Format.getPrettyFormat());
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(saveFile);
			xmlOut.output(doc, fout);
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadGame(File file) {
		if (!file.exists()) startNewGame();
		SAXBuilder builder = new SAXBuilder();
		Document doc;
		try {
			doc = builder.build(file);
			Element root = doc.getRootElement();

			Element head = root.getChild("Head");
			this.currentLevelNumber = head.getAttribute("level").getIntValue();
			setLevel(currentLevelNumber);
			currentLevel.setScore(head.getAttribute("score").getIntValue());

			Element data = root.getChild("LevelData");
			Element[] children = data.getChildren().toArray(new Element[0]);

			Gem[][] gems = new Gem[currentLevel.getPlayAreaWidth()][currentLevel.getPlayAreaHeight()];

			for (Element child : children) {
				int x = child.getAttribute("x").getIntValue();
				int y = child.getAttribute("y").getIntValue();
				int color = child.getAttribute("color").getIntValue();
				boolean remove = child.getAttribute("remove").getBooleanValue();
				gems[x][y] = new Gem(color);
				gems[x][y].remove = remove;
			}
			currentLevel.setTileData(gems);
			ready = true;
		} catch (Exception e) {
			e.printStackTrace();
			startNewGame();
		}
	}

	public void update() {
	    if (!ready) return;
		if (currentLevel != null) {
			currentLevel.update();
		}
	}
	
	public void render() {
	    if (!ready) {
	    	if (multiplayer && multiplayerHost != null) {
	    		// We are hosting a game.
	    		if (!multiplayerHost.isClientConnected()) {
	    			// Nobody has connected yet, display the relevant connection info.
				} else {
	    		    ready = true;
                }
			} else if (multiplayer && multiplayerJoin != null) {
	    		//
			}
	    	return;
		}
		if (currentLevel != null) {
			currentLevel.render();
		}
	}

	public void displose() {
		if (currentLevel != null) {
			currentLevel.displose();
		}
	}

	private void setLevel(int level) {
	    if (level == 0) {
	        this.currentLevel = new Level1();
        }
	}
	
}
