package dnet.gamestudio.columns;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import dnet.gamestudio.columns.log.Log;
import dnet.gamestudio.columns.sound.SoundEngine;
import org.lwjgl.input.Keyboard;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import dnet.gamestudio.columns.game.Game;
import dnet.gamestudio.columns.gui.Gui;
import dnet.gamestudio.columns.gui.GuiPaused;
import dnet.gamestudio.columns.gui.GuiTitle;

import javax.imageio.ImageIO;

public class Columns {

	public static Columns instance;
    public static boolean multiplayerEnabled = true;

	/** The width of the LWJGL Display. */
	public static final int WIDTH = 864;
	/** The height of the LWJGL Display. */
	public static final int HEIGHT = 600;
	
	private SoundEngine soundManager;

	private Gui currentGui;
	private Game currentGame;
	
	private boolean paused = false;
	private boolean pauseChanged = false;
	private boolean running = false;

	private boolean waitForSoundSelect = true;
	
	private Columns() {
		instance = this;
        try {
            setupDisplay();
        } catch (Exception e) {
            Log.f("Setting up display failed with exception:", e);
            return;
        }
        try {
            setupAudio();
        } catch (Exception e) {
            Log.e("Setting up sound failed with exception:", e);
        }
		start();
	}

    private void setupDisplay() throws Exception {
        Log.i("Setting up display...");
        setDisplayIcon();
        Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
        Display.setTitle("D-Net Columns 1.0");
        Display.setVSyncEnabled(true);
        Display.create();
    }

	private void setDisplayIcon() {
		try {
			ByteBuffer[] iconBuffers = new ByteBuffer[4];
			iconBuffers[0] = loadIcon(Columns.class.getResourceAsStream("res/icon/icn_16.png"));
			iconBuffers[1] = loadIcon(Columns.class.getResourceAsStream("res/icon/icn_32.png"));
			iconBuffers[2] = loadIcon(Columns.class.getResourceAsStream("res/icon/icn_64.png"));
			iconBuffers[3] = loadIcon(Columns.class.getResourceAsStream("res/icon/icn_128.png"));
			Display.setIcon(iconBuffers);
		} catch (Exception e) {
			Log.e("Setting display icon failed with exception:", e);
		}
	}

    private void setupAudio() throws Exception {
        Log.i("Setting up sound...");
        AL.create();
        soundManager = new SoundEngine();
        if (soundManager.loadALData() != 1) {
            Log.e("Loading AL Data failed");
            throw new Exception("Sound setup failed: " + AL10.alGetString(AL10.alGetError()));
        }
        soundManager.setListenerValues();
       // soundManager.play(0);
    }

	private ByteBuffer loadIcon(InputStream inputStream) throws IOException {
		if (inputStream == null) throw new NullPointerException("InputStream is null!");
		BufferedImage bufferedImage = ImageIO.read(inputStream);
		int[] aint = bufferedImage.getRGB(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), (int[]) null, 0, bufferedImage.getWidth());
		ByteBuffer byteBuffer = ByteBuffer.allocate(4 * aint.length);
		for (int i : aint) {
			byteBuffer.putInt(i << 8 | i >> 24 & 255);
		}
		byteBuffer.flip();
		try {
			inputStream.close();
		} catch (Exception e) {
			Log.w("Failed to close input stream with exception:", e);
		}
		return byteBuffer;
	}

	private void start() {
	    Log.i("Starting columns...");

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, WIDTH, HEIGHT, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		
		glEnable(GL_TEXTURE_2D);
		
		//currentLevel = new Level1();
		currentGui = new GuiTitle();
		
		running = true;

		Log.i("Entering logic loop.");
		while (!Display.isCloseRequested() && running) {
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			Keyboard.poll();

			if (waitForSoundSelect) {
                Gui.fontRendererLarge.drawString("Do you want sound?", 300, 297);
                Gui.fontRendererLarge.drawString("Y (yes), N (no)", 325, 297 + 19);
                if (Keyboard.isKeyDown(Keyboard.KEY_Y)) {
                    soundManager.setSoundEnabled(true);
                    soundManager.play(SoundEngine.BGM_0);
                    waitForSoundSelect = false;
                } else if (Keyboard.isKeyDown(Keyboard.KEY_N)) {
                    soundManager.setSoundEnabled(false);
                    waitForSoundSelect = false;
                }
            } else {
                if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) && !paused && currentGame != null) {
                    if (!pauseChanged) {
                        paused = true;
                        currentGui = new GuiPaused();
                        pauseChanged = true;
                    }
                } else if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) && paused) {
                    if (!pauseChanged) {
                        paused = false;
                        currentGui = null;
                        pauseChanged = true;
                    }
                } else {
                    pauseChanged = false;
                }

                if (currentGui != null) {
                    currentGui.update();
                    if (currentGui != null) {
                        currentGui.render();
                    }
                }

                if (currentGame != null) {
                    if (!paused) {
                        currentGame.update();
                    }
                    currentGame.render();

                    if (paused) {
                        currentGui.render();
                    }
                }
            }
			
			Display.update();
			Display.sync(60);
		}
		Log.i("Logic loop ended. Shutting down...");
		if (currentGame != null) {
			currentGame.saveGame();
			currentGame.displose();
		}
		AL.destroy();
		Display.destroy();
		Log.i("Goodbye");
		System.exit(0);
	}

	public void startGame() {
		currentGame = new Game();
		currentGame.startNewGame();
		currentGui = null;
	}
	
	public void loadGame(File saveFile) {
		currentGame = new Game(saveFile);
		currentGui = null;
	}
	
	public void hostGame() {
		currentGame = new Game();
		currentGame.hostGame();
		currentGui = null;
	}
	
	public void joinGame(String host, int port) {
		currentGame = new Game();
		currentGame.joinGame(host, port);
		currentGui = null;
	}
	
	public void stop() {
		running = false;
	}

	public SoundEngine getSoundEngine() {
		return soundManager;
	}

	public static void main(String[] args) {
		System.setProperty("org.lwjgl.librarypath", new File("natives").getAbsolutePath());
		System.setProperty("java.library.path", new File("natives").getAbsolutePath());
		System.setProperty("net.java.games.input.librarypath", new File("natives").getAbsolutePath());
		new Columns();
	}

}
