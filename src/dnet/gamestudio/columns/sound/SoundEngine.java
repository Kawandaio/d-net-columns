package dnet.gamestudio.columns.sound;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.WaveData;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.openal.AL10.*;

public class SoundEngine {

    public static final int NUM_BUFFERS = 5;
    public static final int NUM_SOURCES = 5;

    public static final int BGM_0 = 0;
    public static final int SHUFFLE = 1;
    public static final int MOVE = 2;
    public static final int PLACE = 3;
    public static final int DESTROY = 4;

    IntBuffer buffer = BufferUtils.createIntBuffer(NUM_BUFFERS);
    IntBuffer source = BufferUtils.createIntBuffer(NUM_BUFFERS);

    FloatBuffer sourcePos = (FloatBuffer) BufferUtils.createFloatBuffer(3 * NUM_BUFFERS).rewind();
    FloatBuffer sourceVelocity = (FloatBuffer) BufferUtils.createFloatBuffer(3 * NUM_BUFFERS).rewind();

    FloatBuffer listenerPos = (FloatBuffer) BufferUtils.createFloatBuffer(3).put(new float[] {0.0f, 0.0f, 0.0f}).rewind();
    FloatBuffer listenerVelocity = (FloatBuffer) BufferUtils.createFloatBuffer(3).put(new float[] {0.0f, 0.0f, 0.0f}).rewind();
    FloatBuffer listenerOrientation = (FloatBuffer) BufferUtils.createFloatBuffer(6).put(new float[] {0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f}).rewind();

    private boolean soundEnabled = false;

    public SoundEngine() {

    }

    public int loadALData() {
        alGenBuffers(buffer);
        if (alGetError() != AL_NO_ERROR) return AL_FALSE;

        WaveData waveFile = WaveData.create(SoundEngine.class.getResource("bgm.wav"));
        alBufferData(buffer.get(BGM_0), waveFile.format, waveFile.data, waveFile.samplerate);
        waveFile.dispose();

        waveFile = WaveData.create(SoundEngine.class.getResource("Pickup_02.wav"));
        alBufferData(buffer.get(SHUFFLE), waveFile.format, waveFile.data, waveFile.samplerate);
        waveFile.dispose();

        waveFile = WaveData.create(SoundEngine.class.getResource("Collect_Point_01.wav"));
        alBufferData(buffer.get(MOVE), waveFile.format, waveFile.data, waveFile.samplerate);
        waveFile.dispose();

        // TODO Load the other sounds.

        alGenSources(source);
        if (alGetError() != AL_NO_ERROR) return AL_FALSE;

        alSourcei(source.get(BGM_0), AL_BUFFER,   buffer.get(BGM_0));
        alSourcef(source.get(BGM_0), AL_PITCH,    1.0f);
        alSourcef(source.get(BGM_0), AL_GAIN,     1.0f);
        alSource (source.get(BGM_0), AL_POSITION, (FloatBuffer) sourcePos.position(BGM_0 * 3));
        alSource (source.get(BGM_0), AL_VELOCITY, (FloatBuffer) sourceVelocity.position(BGM_0 * 3));
        alSourcei(source.get(BGM_0), AL_LOOPING,  AL_TRUE);

        alSourcei(source.get(SHUFFLE), AL_BUFFER,   buffer.get(SHUFFLE));
        alSourcef(source.get(SHUFFLE), AL_PITCH,    1.0f);
        alSourcef(source.get(SHUFFLE), AL_GAIN,     1.0f);
        alSource (source.get(SHUFFLE), AL_POSITION, (FloatBuffer) sourcePos.position(SHUFFLE * 3));
        alSource (source.get(SHUFFLE), AL_VELOCITY, (FloatBuffer) sourceVelocity.position(SHUFFLE * 3));
        alSourcei(source.get(SHUFFLE), AL_LOOPING,  AL_FALSE);

        alSourcei(source.get(MOVE), AL_BUFFER,   buffer.get(MOVE));
        alSourcef(source.get(MOVE), AL_PITCH,    1.0f);
        alSourcef(source.get(MOVE), AL_GAIN,     1.0f);
        alSource (source.get(MOVE), AL_POSITION, (FloatBuffer) sourcePos.position(MOVE * 3));
        alSource (source.get(MOVE), AL_VELOCITY, (FloatBuffer) sourceVelocity.position(MOVE * 3));
        alSourcei(source.get(MOVE), AL_LOOPING,  AL_FALSE);

        if (alGetError() != AL_NO_ERROR) return AL_FALSE;

        return AL_TRUE;
    }

    public void setListenerValues() {
        alListener(AL_POSITION, listenerPos);
        alListener(AL_VELOCITY, listenerVelocity);
        alListener(AL_ORIENTATION, listenerOrientation);
    }

    public void dispose() {
        alDeleteSources(source);
        alDeleteBuffers(buffer);
    }

    public void play(int soundId) {
        if (!soundEnabled) return;
        alSourceStop(source.get(soundId));
        alSourcePlay(source.get(soundId));
    }

    public void setSoundEnabled(boolean enabled) {
        this.soundEnabled = enabled;
    }

}
