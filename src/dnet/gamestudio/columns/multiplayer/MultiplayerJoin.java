package dnet.gamestudio.columns.multiplayer;

import dnet.gamestudio.columns.log.Log;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class MultiplayerJoin implements Runnable {

	private Socket socket;
	private SocketAddress address;
	private BufferedReader bufferedReader;
	private PrintStream printStream;

	private Thread thread;
	private boolean running = false;

	public MultiplayerJoin(String host, int port) throws Exception {
		address = new InetSocketAddress(host, port);
	    socket = new Socket();
	}

	public void start() {
		running = true;
		thread = new Thread(this);
		thread.start();;
	}

	@Override
	public void run() {
	    try {
            socket.connect(address);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            printStream = new PrintStream(socket.getOutputStream());
            while (running) {

            }
	    } catch (Exception e) {
            Log.f("Network operations encountered an exception:", e);
        }

	}
	
}
