package dnet.gamestudio.columns.multiplayer;

import dnet.gamestudio.columns.game.Game;
import dnet.gamestudio.columns.log.Log;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class MultiplayerHost implements Runnable {

	/** Used to listen on a port for another player. */
	private ServerSocket serverSocket;
	/** The socket that will be connected to the other player. */
	private Socket clientSocket;
	/** Used to read incoming data. */
	private BufferedReader bufferedReader;
	/** Used to write data. */
	private PrintStream printStream;

	/** The network thread. */
	private Thread thread;
	/** If the network listener is running. */
	private boolean running = false;
	/** Reference to the game. */
	private Game game;

	private boolean clientConnected = false;

	public MultiplayerHost(Game game) throws Exception {
		serverSocket = new ServerSocket(9999);
		Log.i("Multiplayer host listening on port: " + getPort());
		this.game = game;
	}
	
	public void start() {
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
	    running = false;
	    try {
	        clientSocket.close();
	        serverSocket.close();
        } catch (Exception e) {
	        e.printStackTrace();
        }
    }
	
	public int getPort() {
		return serverSocket.getLocalPort();
	}

	@Override
	public void run() {
		try {
		    Log.i("Awaiting connection...");
			clientSocket = serverSocket.accept();
			Log.i("Accepted connection");
			bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			printStream = new PrintStream(clientSocket.getOutputStream());
			printStream.println("Welcome");
			clientConnected = true;

			while (running) {
			    if (bufferedReader.ready()) {
			        String input = bufferedReader.readLine();
                }



			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    public boolean isClientConnected() {
        return clientConnected;
    }
}
