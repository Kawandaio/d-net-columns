package dnet.gamestudio.columns.gui;

import dnet.gamestudio.columns.Columns;
import dnet.gamestudio.columns.game.Column;
import dnet.gamestudio.columns.res.TextureLoader;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.input.Keyboard;

import java.io.File;

public class GuiTitle extends Gui {

	private final String[] options = new String[] {"New Game", "Load Game", "Host Multiplayer", "Join Multiplayer", "Quit"};

	private int selectedOption = 0;
	private boolean[] keystates = new boolean[] {false, false, false};

	private boolean canLoad = false;

	public GuiTitle() {
		x = 0;
		y = 0;
		width = 864;
		height = 600;
		TextureLoader.loadTexture("title.png");
		if ((new File("columns.sav")).exists()) {
			canLoad = true;
		}
	}
	
	@Override
	public void update() {
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			if (!keystates[0]) {
				keystates[0] = true;
				if (selectedOption == options.length -1) return;
				selectedOption ++;
				if (selectedOption == 1 && !canLoad) {
					selectedOption ++;
				}
				if ((selectedOption == 2 || selectedOption == 3) && !Columns.multiplayerEnabled) {
					selectedOption = 4;
				}
			}
		} else {
			keystates[0] = false;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			if (!keystates[1]) {
				keystates[1] = true;
				if (selectedOption == 0) return;
				selectedOption --;
				if (selectedOption == 1 && !canLoad) {
					selectedOption --;
				}
				if ((selectedOption == 2 || selectedOption == 3) && !Columns.multiplayerEnabled) {
					selectedOption = 1;
				}
			}
		} else {
			keystates[1] = false;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
			if (!keystates[2]) {
				keystates[2] = true;
				handleSelection();
			}
		} else {
			keystates[2] = false;
		}
	}

	private void handleSelection() {
		switch (selectedOption) {
		case 0:
			Columns.instance.startGame();
			break;
		case 1:
			Columns.instance.loadGame(new File("columns.sav"));
			break;
		case 2:
			if (!Columns.multiplayerEnabled) break;
			Columns.instance.hostGame();
			break;
		case 3:
			if (!Columns.multiplayerEnabled) break;
			Columns.instance.joinGame("localhost", 9999);
			break;
		case 4:
			Columns.instance.stop();
			break;
		}
	}

	@Override
	public void render() {
		glBindTexture(GL_TEXTURE_2D, TextureLoader.getTexture("title.png"));
		glBegin(GL_QUADS);
			glTexCoord2d(0, 0);
			glVertex2i(0, 0);
			
			glTexCoord2d(0.844d, 0);
			glVertex2i(865, 0);
			
			glTexCoord2d(0.844d, 0.586d);
			glVertex2i(865, 600);
			
			glTexCoord2d(0, 0.586d);
			glVertex2i(0, 600);
		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
		
		Gui.fontRendererLarge.drawString("D-Net Columns", (864 / 2) - 95, 56);
		
		for (int i = 0; i < options.length; i ++) {
			if (i == 1 && !canLoad) {
				glColor3f(0.4f, 0.4f, 0.4f);
			} else if ((i == 2 || i == 3) && !Columns.multiplayerEnabled) {
				glColor3f(0.4f, 0.4f, 0.4f);
			} else {
				if (selectedOption == i) {
					glColor3f(0.0f, 1.0f, 0.24f);
				}
			}

			Gui.fontRendererLarge.drawString(options[i], 75, 256 + (24 * i));
			glColor3f(1.0f, 1.0f, 1.0f);
		}
		
	}

}
