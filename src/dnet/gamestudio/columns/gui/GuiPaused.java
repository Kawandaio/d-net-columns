package dnet.gamestudio.columns.gui;

import static org.lwjgl.opengl.GL11.*;

public class GuiPaused extends Gui {

	private long lastUpdate = 0;
	
	private int yOffs = 0;
	
	public GuiPaused() {
		x = (864 / 2) - (200 / 2);
		y = (600 / 2) - (35 / 2);
		width = 200;
		height = 35;
	}
	
	@Override
	public void update() {
		long time = System.currentTimeMillis();
		if (time - lastUpdate > 250) {
			lastUpdate = time;
			if (yOffs == 3) {
				yOffs = 0;
			} else if (yOffs == 0) {
				yOffs = 3;
			}
		}
	}

	@Override
	public void render() {
		glColor4f(0, 0, 0, 0.9f);
		glBegin(GL_QUADS);
			glVertex2i(x, y);
			glVertex2i(x + width, y);
			glVertex2i(x + width, y + height);
			glVertex2i(x, y + height);
		glEnd();
		glColor4f(1, 1, 1, 1);
		fontRendererLarge.drawString("Paused", 384, 293 + yOffs);
	}

}
