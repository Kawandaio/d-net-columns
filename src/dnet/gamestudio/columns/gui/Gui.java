package dnet.gamestudio.columns.gui;

public abstract class Gui {

	public static final FontRenderer fontRendererSmall = new FontRenderer(8, "verifier_font_8x8.png");
	public static final FontRenderer fontRendererLarge = new FontRenderer(16, "kromagrad_16x16.png");	
	
	protected int x;
	protected int y;
	protected int width;
	protected int height;
	
	public abstract void update();
	public abstract void render();
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
}
