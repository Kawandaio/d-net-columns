package dnet.gamestudio.columns.gui;

import dnet.gamestudio.columns.res.TextureLoader;

import static org.lwjgl.opengl.GL11.*;

public class FontRenderer {

	private final char[] FONT_CHARS = new char[] {'!', '"', '#', '$', '%', '^', '\'', '(', ')', '_', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';',
			'<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	private int fontSize = 16;
	
	private String textureName;
	
	public FontRenderer(int size, String texture) {
		this.fontSize = size;
		textureName = texture;
		//fontTexture = TextureLoader.loadTexture("kromagrad_16x16.png");
		TextureLoader.loadTexture(texture);
	}
	
	public void drawString(String string) {
		
	}

	public void drawString(String string, int x, int y) {
		char[] chars = string.toUpperCase().toCharArray();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, TextureLoader.getTexture(textureName));
		glBegin(GL_QUADS);
		int offset = 0;
		int yOffset = 0;
		for (char c : chars) {
			for (int i = 0; i < FONT_CHARS.length; i ++) {
				if (FONT_CHARS[i] == c) {
					drawChar(i + 1, x + offset, y + yOffset);
					offset += fontSize + 1;
				}
			}
			if (Character.isWhitespace(c)) {
				if (fontSize == 8) {
					offset += 3;
				} else {
					offset += 6;
				}
			}
			if (c == '\n') {
				offset = 0;
				yOffset += 20;
			}
		}
		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	private void drawChar(int charIndex, int x, int y) {
		glTexCoord2d((0.0169491525423729d * charIndex), 0);
		glVertex2i(x, y);
		
		glTexCoord2d((0.0169491525423729d * charIndex) + 0.0169491525423729d, 0);
		glVertex2i(x + fontSize, y);
		
		glTexCoord2d((0.0169491525423729d * charIndex) + 0.0169491525423729d, 1);
		glVertex2i(x + fontSize, y + fontSize);
		
		glTexCoord2d((0.0169491525423729d * charIndex), 1);
		glVertex2i(x, y + fontSize);
	}
	
}
