package dnet.gamestudio.columns.log;

/**
 * Created by Kawandaio (Guy Delph) on 28/02/2017.
 */
public class Log {

    public static void v(String message, Object o) {
        System.out.print("VALUE: " + message + ": ");
        System.out.println(o);
    }

    public static void i(String message) {
        System.out.println("INFO: " + message);
    }

    public static void e(String error) {
        System.err.println("ERROR: " + error);
    }

    public static void e(String error, Throwable t) {
        e(error);
        t.printStackTrace();
    }

    public static void w(String warning) {
        System.err.println("WARNING: " + warning);
    }

    public static void w(String warning, Throwable t) {
        w(warning);
        t.printStackTrace();
    }

    public static void f(String fatal) {
        System.err.println("FATAL: " + fatal);
    }

    public static void f(String fatal, Throwable t) {
        f(fatal);
        t.printStackTrace();
    }

}
